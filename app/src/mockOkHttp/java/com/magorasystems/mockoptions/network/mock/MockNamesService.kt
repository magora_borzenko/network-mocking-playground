package com.magorasystems.mockoptions.network.mock

import android.content.Context
import com.magorasystems.mockoptions.BuildConfig
import com.magorasystems.mockoptions.model.Person
import com.magorasystems.mockoptions.network.NamesService
import kotlinx.coroutines.Deferred
import kotlinx.coroutines.runBlocking


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
class MockNamesService(
    context: Context,
    private val service: NamesService
) : MockService(context, BuildConfig.NAMES_SERVICE_BASE_URL), NamesService {
    override fun getPeopleAsync(amount: Int): Deferred<List<Person>> = runBlocking {
        mocked(
            200,
            "get_people_success.json"
        ) {
            service.getPeopleAsync(amount)
        }
    }
}