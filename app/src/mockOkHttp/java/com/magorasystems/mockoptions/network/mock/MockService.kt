package com.magorasystems.mockoptions.network.mock

import android.content.Context
import com.magorasystems.util.FileUtils
import kotlinx.coroutines.delay
import okhttp3.mockwebserver.MockResponse
import okhttp3.mockwebserver.MockWebServer
import java.io.IOException
import java.net.InetAddress
import java.net.URL


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
open class MockService(
    protected val context: Context,
    baseUrl: String
) {
    protected val url = URL(baseUrl)

    protected suspend inline fun <T> mocked(
        responseCode: Int,
        fileName: String,
        delayMillis: Long = DEFAULT_DELAY_MS,
        crossinline block: () -> T
    ): T =
        MockWebServer().use { ws ->
            ws.start(InetAddress.getByName(url.host), url.port)
            ws.enqueue(
                response(
                    responseCode,
                    fileName
                )
            )

            val result = block()

            delay(delayMillis)

            ws.takeRequest()
            ws.shutdown()

            return result
        }

    protected fun response(
        responseCode: Int,
        fileName: String
    ): MockResponse {
        try {
            val body = FileUtils.getStringFromAssetsFile(
                context,
                String.format(
                    "network_mock_responses/%s",
                    fileName
                )
            )

            return MockResponse().apply {
                setResponseCode(responseCode)
                setBody(body)
            }
        } catch (e: IOException) {
            e.printStackTrace()
        }

        throw RuntimeException()
    }

    companion object {
        protected const val DEFAULT_DELAY_MS: Long = 2000 // Network calls will take 2 seconds
    }
}