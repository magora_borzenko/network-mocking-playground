package com.magorasystems.util;

import android.content.Context;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

/**
 * Created at Magora Systems (http://magora-systems.com) on 21.09.16
 *
 * @author Stanislav S. Borzenko
 */

@SuppressWarnings("WeakerAccess")
public class FileUtils {
    public static String convertStreamToString(InputStream is)
            throws IOException {
        StringBuilder sb = new StringBuilder();
        String line;

        BufferedReader reader = new BufferedReader(new InputStreamReader(is));

        while ((line = reader.readLine()) != null) {
            sb.append(line).append("\n");
        }

        reader.close();

        return sb.toString();
    }

    public static String getStringFromAssetsFile(Context context, String filePath)
            throws IOException {
        final InputStream stream =
                context.getResources().getAssets().open(filePath);

        String string = convertStreamToString(stream);

        stream.close();

        return string;
    }
}