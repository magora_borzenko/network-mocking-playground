package com.magorasystems.mockoptions.network

import android.content.Context
import com.magorasystems.mockoptions.network.mock.MockNamesService
import retrofit2.Retrofit
import retrofit2.mock.MockRetrofit
import retrofit2.mock.NetworkBehavior


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
object NamesServiceFactory {
    fun getService(
        @Suppress("UNUSED_PARAMETER") context: Context,
        retrofit: Retrofit
    ): NamesService {
        val mockRetrofit = MockRetrofit.Builder(retrofit)
            .networkBehavior(
                NetworkBehavior.create().apply {
                    setFailurePercent(0)
                })
            .build()

        return MockNamesService(
            context,
            mockRetrofit.create(NamesService::class.java)
        )
    }
}