package com.magorasystems.mockoptions.network.mock

import android.content.Context
import com.google.gson.GsonBuilder
import com.google.gson.reflect.TypeToken
import com.magorasystems.util.FileUtils
import kotlinx.coroutines.Deferred
import okhttp3.MediaType
import okhttp3.ResponseBody
import retrofit2.Response
import retrofit2.mock.BehaviorDelegate
import retrofit2.mock.Calls
import java.io.IOException
import java.lang.reflect.Type


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
open class MockService<S>(
    protected val context: Context,
    protected val behaviorDelegate: BehaviorDelegate<S>
) {
    protected inline fun <reified R : Any> mocked(
        responseCode: Int,
        fileName: String,
        block: (service: S) -> Deferred<R>
    ): Deferred<R> {
        return mock(
            context,
            behaviorDelegate,
            responseCode,
            fileName,
            block
        )
    }

    protected inline fun <reified R : Any> mock(
        context: Context,
        behaviorDelegate: BehaviorDelegate<S>,
        responseCode: Int,
        fileName: String,
        block: (service: S) -> Deferred<R>
    ): Deferred<R> {
        return block(
            mockService(
                context,
                behaviorDelegate,
                typeOf<R>(),
                responseCode,
                fileName
            )
        )
    }

    protected fun mockService(
        context: Context,
        behaviorDelegate: BehaviorDelegate<S>,
        type: Type?,
        responseCode: Int,
        fileName: String
    ): S {
        try {
            val json = FileUtils.getStringFromAssetsFile(
                context,
                String.format(
                    "network_mock_responses/%s",
                    fileName
                )
            )

            val response: Response<Any> =
                if (responseCode in 200 until 300) {
                    Response.success(
                        GsonBuilder()
                            .create()
                            .fromJson<Any>(json, type)
                    )
                } else {
                    Response.error(
                        responseCode,
                        ResponseBody.create(
                            MediaType.parse("application/json"),
                            json
                        )
                    )
                }

            return behaviorDelegate.returning(Calls.response(response))
        } catch (e: IOException) {
            e.printStackTrace()
        }

        throw RuntimeException()
    }
}

inline fun <reified T> typeOf(): Type {
    return object : TypeToken<T>() {}.type
}