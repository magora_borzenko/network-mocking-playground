package com.magorasystems.mockoptions.network.mock

import android.content.Context
import com.magorasystems.mockoptions.model.Person
import com.magorasystems.mockoptions.network.NamesService
import kotlinx.coroutines.Deferred
import retrofit2.mock.BehaviorDelegate


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
class MockNamesService(
    context: Context,
    behaviorDelegate: BehaviorDelegate<NamesService>
) : MockService<NamesService>(context, behaviorDelegate), NamesService {
    override fun getPeopleAsync(amount: Int): Deferred<List<Person>> {
        return mocked(
            200,
            "get_people_success.json"
        ) {
            return it.getPeopleAsync(amount)
        }
    }
}