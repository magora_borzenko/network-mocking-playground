package com.magorasystems.mockoptions.network

import android.content.Context
import retrofit2.Retrofit


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
object NamesServiceFactory {
    fun getService(
        @Suppress("UNUSED_PARAMETER") context: Context,
        retrofit: Retrofit): NamesService {
        return retrofit.create(NamesService::class.java)
    }
}