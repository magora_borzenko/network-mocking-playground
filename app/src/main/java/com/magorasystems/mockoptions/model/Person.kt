package com.magorasystems.mockoptions.model


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
data class Person(
    val name: String,
    val surname: String,
    val gender: String,
    val region: String
)