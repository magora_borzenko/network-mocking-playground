package com.magorasystems.mockoptions

import com.magorasystems.mockoptions.model.Person


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
interface PeopleView {
    fun showProgress()
    fun showPeople(people: List<Person>)
    fun showError(errorDescription: String)
}