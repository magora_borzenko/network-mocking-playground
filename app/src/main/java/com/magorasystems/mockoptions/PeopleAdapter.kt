package com.magorasystems.mockoptions

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.magorasystems.mockoptions.model.Person
import kotlinx.android.synthetic.main.item_person.view.*


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
class PeopleAdapter : RecyclerView.Adapter<PeopleAdapter.ViewHolder>() {
    private var people: List<Person>? = null

    fun setPeople(people: List<Person>?) {
        this.people = people
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PeopleAdapter.ViewHolder {
        LayoutInflater.from(parent.context)
            .inflate(R.layout.item_person, parent, false).let {
                return ViewHolder(it)
            }
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val person = people!![position]

        holder.firstNameTextView.text = person.name
        holder.lastNameTextView.text = person.surname

        holder.itemView.setBackgroundColor(
            if (position.even()) {
                Color.WHITE
            } else {
                Color.parseColor("#cccccc")
            }
        )
    }

    override fun getItemCount() = people?.size ?: 0

    class ViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val firstNameTextView = itemView.firstNameTextView!!
        val lastNameTextView = itemView.lastNameTextView!!
    }

    private fun Int.even(): Boolean = this % 2 == 0
}