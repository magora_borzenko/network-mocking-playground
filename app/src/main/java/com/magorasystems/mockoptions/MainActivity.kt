package com.magorasystems.mockoptions

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.magorasystems.mockoptions.model.Person
import com.magorasystems.mockoptions.network.DefaultPeopleProvider
import com.magorasystems.mockoptions.network.NamesServiceFactory
import com.magorasystems.mockoptions.network.RetrofitFactory
import kotlinx.android.synthetic.main.activity_main.*

/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
class MainActivity : AppCompatActivity(), PeopleView {
    private val presenter = MainPresenter(
        DefaultPeopleProvider(
            NamesServiceFactory.getService(
                this,
                RetrofitFactory.newInstance())
        )
    )

    private val peopleAdapter = PeopleAdapter()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        with(peopleRecyclerView) {
            layoutManager = LinearLayoutManager(this@MainActivity)
            adapter = peopleAdapter
        }

        lifecycle.addObserver(presenter)
    }

    override fun onStart() {
        super.onStart()

        presenter.attachView(this)
    }

    // region PeopleView
    override fun showProgress() {
        setUiState(UiState.LOADING)
    }

    override fun showPeople(people: List<Person>) {
        setUiState(UiState.CONTENT)
        peopleAdapter.setPeople(people)
    }

    override fun showError(errorDescription: String) {
        setUiState(UiState.ERROR)
        errorTextView.text = errorDescription
    }
    // endregion

    private fun setUiState(state: UiState) {
        when (state) {
            UiState.LOADING -> {
                peopleLoadingProgress.visibility = View.VISIBLE
                peopleRecyclerView.visibility = View.GONE
                errorTextView.visibility = View.GONE
            }

            UiState.CONTENT -> {
                peopleLoadingProgress.visibility = View.GONE
                peopleRecyclerView.visibility = View.VISIBLE
                errorTextView.visibility = View.GONE
            }

            UiState.ERROR -> {
                peopleLoadingProgress.visibility = View.GONE
                peopleRecyclerView.visibility = View.GONE
                errorTextView.visibility = View.VISIBLE
            }
        }
    }

    private enum class UiState {
        LOADING,
        CONTENT,
        ERROR
    }
}
