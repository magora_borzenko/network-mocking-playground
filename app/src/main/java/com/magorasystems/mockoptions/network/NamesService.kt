package com.magorasystems.mockoptions.network

import com.magorasystems.mockoptions.model.Person
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Query

/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
interface NamesService {
    @GET("api/")
    fun getPeopleAsync(@Query("amount") amount: Int): Deferred<List<Person>>
}