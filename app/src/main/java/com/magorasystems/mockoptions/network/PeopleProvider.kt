package com.magorasystems.mockoptions.network

import com.magorasystems.mockoptions.model.Person


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
interface PeopleProvider {
    suspend fun getPeople(amount: Int): List<Person>
}