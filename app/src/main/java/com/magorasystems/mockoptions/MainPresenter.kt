package com.magorasystems.mockoptions

import android.util.Log
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import com.magorasystems.mockoptions.network.PeopleProvider
import kotlinx.coroutines.*
import kotlin.coroutines.CoroutineContext


/**
 * Created by Magora Systems (magora-systems.com) in 2019.
 *
 * @author Stanislav S. Borzenko
 */
class MainPresenter(
    private val peopleProvider: PeopleProvider
) : LifecycleObserver, CoroutineScope {
    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main + job

    private var view: PeopleView? = null

    private lateinit var job: Job

    private val exHandler = CoroutineExceptionHandler { _, ex ->
        log("=== error ===")
        log(ex.localizedMessage)

        view?.showError(ex.localizedMessage)
    }

    @Suppress("unused")
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    private fun onStart() {
        job = Job()

        loadPeople()
    }

    private fun loadPeople() = launch(exHandler) {
        log("loadPeople() called()")

        view?.showProgress()

        val people = async(Dispatchers.IO) {
            log("async getPeople() called()")
            peopleProvider.getPeople(25)
        }

        log("people.await() called()")
        view?.showPeople(people.await())
    }

    @OnLifecycleEvent(Lifecycle.Event.ON_STOP)
    fun detachView() {
        view = null

        job.cancel()
    }

    fun attachView(view: PeopleView) {
        this.view = view
    }

    private fun log(message: String) {
        Log.d(TAG, "[${Thread.currentThread().name}] $message")
    }

    companion object {
        private const val TAG = "MainPresenter"
    }
}