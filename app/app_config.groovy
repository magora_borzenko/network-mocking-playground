environments {
    prod {
        baseUrl = 'https://uinames.com'
    }

    dev {
        baseUrl = 'https://uinames.com'
    }

    mock {
        baseUrl = 'http://localhost:8888'
    }
}